<?php

namespace Feedback\AdminBundle\Datatables;

use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class BusinessDatatable
 *
 * @package Feedback\AdminBundle\Datatables
 */
class BusinessDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatableView()
    {
        $this->features->setFeatures(array(
            "auto_width" => true,
            "defer_render" => false,
            "info" => true,
            "jquery_ui" => false,
            "length_change" => true,
            "ordering" => true,
            "paging" => true,
            "processing" => true,
            "scroll_x" => false,
            "scroll_y" => "",
            "searching" => true,
            "server_side" => true,
            "state_save" => false,
            "delay" => 0
        ));

                $this->ajax->setOptions(array(
            "url" => $this->router->generate("business_results"),
            "type" => "GET"
        ));
        
        $this->options->setOptions(array(
            "display_start" => 0,
            "dom" => "lfrtip",
            "length_menu" => array(10, 25, 50, 100),
            "order_classes" => true,
            "order" => [[0, "asc"]],
            "order_multi" => true,
            "page_length" => 10,
            "paging_type" => Style::FULL_NUMBERS_PAGINATION,
            "renderer" => "",
            "scroll_collapse" => false,
            "search_delay" => 0,
            "state_duration" => 7200,
            "stripe_classes" => array(),
            "responsive" => false,
            "class" => Style::BASE_STYLE,
            "individual_filtering" => false,
            "individual_filtering_position" => "foot",
            "use_integration_options" => false
        ));

        $this->columnBuilder
                ->add("name", "column", array("title" => "Name",))
                ->add("latitude", "column", array("title" => "Latitude",))
                ->add("longitude", "column", array("title" => "Longitude",))
                ->add("appleMapId", "column", array("title" => "AppleMapId",))
                ->add("googleMapId", "column", array("title" => "GoogleMapId",))
                ->add("createdDate", "datetime", array("title" => "CreatedDate",))
                ->add("updatedDate", "datetime", array("title" => "UpdatedDate",))
                ->add("deletedDate", "datetime", array("title" => "DeletedDate",))
                ->add("address1", "column", array("title" => "Address1",))
                ->add("address2", "column", array("title" => "Address2",))
                ->add("city", "column", array("title" => "City",))
                ->add("state", "column", array("title" => "State",))
                ->add("zip", "column", array("title" => "Zip",))
                ->add("phone", "column", array("title" => "Phone",))
                ->add("id", "column", array("title" => "Id",))
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return "Feedback\AdminBundle\Entity\Business";
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "business_datatable";
    }
}
