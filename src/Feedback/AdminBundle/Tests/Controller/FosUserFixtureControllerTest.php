<?php
/**
 * Created by Justin St. Germain
 * Date: 6/9/15
 * Time: 8:15 PM
 */

namespace Feedback\AdminBundle\Tests\Controller;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Feedback\AdminBundle\DataFixtures\ORM\LoadDefaultUsers;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FosUserFixtureControllerTest extends WebTestCase
{

	public function testFosUserDefault()
	{
		$client = self::createClient();
		$container = $client->getKernel()->getContainer();
        $em = $container->get('doctrine')->getManager();
 
        // Purge tables
		$purger = new ORMPurger();
        $executor = new ORMExecutor($em, $purger);
        $executor->purge();
 
        // Load fixtures
        $loader = new Loader();
        $fixtures = new LoadDefaultUsers();
        $fixtures->setContainer($container);
        $loader->addFixture($fixtures);
        $executor->execute($loader->getFixtures());
	}

}
