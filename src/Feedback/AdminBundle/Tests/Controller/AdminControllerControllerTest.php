<?php

namespace Feedback\AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerControllerTest extends WebTestCase
{
    public function testAdmin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin');
    }

    public function testDashboard()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/dashboard');
    }

    public function testMybusinesses()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/businesses');
    }

}
