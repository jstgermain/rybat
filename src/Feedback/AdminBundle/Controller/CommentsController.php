<?php
/**
 * Created by Justin St. Germain
 * Date: 6/9/15
 * Time: 1:00 PM
 */

namespace Feedback\AdminBundle\Controller;

use Feedback\AdminBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommentsController extends Controller
{

	public function indexAction($businessId)
	{

		$comments = $this->getDoctrine()
			->getRepository('AdminBundle:Comment')
			->getAllCommentsByBusinessId($businessId);

//	    var_dump($comments); die;

//	    foreach ( $comments as $comment )
//	    {
//		    echo $comment->getMessage() . '<br/>';
//	    } die;


		return $this->render('AdminBundle:Comments:index.html.twig', array(
			'comments' => $comments,
		));
	}

}