<?php

namespace Feedback\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AdminControllerController extends Controller
{
    public function adminAction()
    {

	    if (false === $this->get('security.context')->isGranted(
			    'IS_AUTHENTICATED_REMEMBERED'
		    )) {
		    throw new AccessDeniedException();
	    }

        return $this->render('AdminBundle:AdminController:admin.html.twig', array(
            // ...
        ));
    }

    public function myBusinessesAction()
    {
        return $this->render('AdminBundle:AdminController:businesses.html.twig', array(
            // ...
        ));
    }

}
