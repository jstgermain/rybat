<?php

namespace Feedback\AdminBundle\Controller;

use Feedback\AdminBundle\Entity\UserDetails;
use Feedback\AdminBundle\Form\UserDetailsType;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\FOSUserEvents;
use FOS\RestBundle\View\View;

class ProfileController extends BaseController
{

	/**
	 * Override default FOSUserBundle Profile Edit
	 */
    public function editAction(Request $request)
    {

	    $user = $this->getUser();
	    if (!is_object($user) || !$user instanceof UserInterface) {
		    throw new AccessDeniedException('This user does not have access to this section.');
	    }

	    /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
	    $dispatcher = $this->get('event_dispatcher');

	    $event = new GetResponseUserEvent($user, $request);
	    $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

	    if (null !== $event->getResponse()) {
		    return $event->getResponse();
	    }

	    //Create a new user details entity instance
	    $userDetails = new UserDetails();
	    $userDetails->setFosUser($user);
	    $editUserDetails = $this->createForm(new UserDetailsType(), $userDetails);

	    return $this->render('AdminBundle:Profile:edit.html.twig', array(
		    'user_details'  => $editUserDetails->createView()
        ));

    }


	/**
	 * Manage All Users
	 * Puts all users in a list to select individual user to edit
	 */
	public function manageUsersAction()
	{

		if (false === $this->get('security.context')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$users = $this->getDoctrine()
			->getRepository('AdminBundle:UserDetails')
			->getAllUsersWithDetails();

		return $this->render('AdminBundle:Profile:manageUsers.html.twig', array(
			'users'    => $users,
		));

	}


	/**
	 * Manage A Single User
	 */
	public function manageUserByIdAction($userId)
	{

		if (false === $this->get('security.context')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$user = $this->getDoctrine()
			->getRepository('AdminBundle:UserDetails')
			->getUserDetailsById($userId);

//		var_dump($user); die;
//		echo $user->getId(); die;

		return $this->render('AdminBundle:Profile:manageUser.html.twig', array(
			'user'      => $user
		));

	}


	/**
	 * Edit A Single User
	 */
	public function editUserByIdAction($userId)
	{

		if (false === $this->get('security.context')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$em = $this->getDoctrine()->getEntityManager();
		$editprofile = $em->getRepository('AdminBundle:UserDetails')->findOneBy(['fosUser' => $userId]);
		$form = $this->createForm(new UserDetailsType(), $editprofile);

		return $this->render('AdminBundle:Profile:editUser.html.twig', array(
			'user_details'  => $form->createView(),
			'user_id'       => $userId
		));

	}

	public function getUserListAction()
	{
		if (false === $this->get('security.authorization_checker')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$users = $this->getDoctrine()
			->getRepository('AdminBundle:UserDetails')
			->getAllUsernames();

		foreach($users as $user)
		{
			$u[] = $user['username'];
		}

		return new JsonResponse($u);

	}


}
