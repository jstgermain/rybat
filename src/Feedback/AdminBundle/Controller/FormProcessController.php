<?php
/**
 * Created by Justin St. Germain
 * Date: 6/12/15
 * Time: 8:43 PM
 */

namespace Feedback\AdminBundle\Controller;

use Feedback\AdminBundle\Entity\UserDetails;
use Feedback\AdminBundle\Form\UserDetailsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;

class FormProcessController extends FOSRestController
{

	public  function updateMyUserDetailsAction(Request $request)
	{

		$entity = new UserDetails();
		$form = $this->createForm(new UserDetailsType(), $entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();

			$user = $em->getRepository('AdminBundle:UserDetails')
				->findOneBy(['fosUser' => $this->getUser()]);

			if (!$user instanceof UserDetails) {
				$entity->setFirstName($form->get('firstName')->getData());
				$entity->setLastName($form->get('lastName')->getData());
				$entity->setPhone($form->get('phone')->getData());
				$entity->setFosUser($this->getUser());
				$entity->setCreatedDate(new \DateTime());
				$em->persist($entity);
				$em->flush();
			} else {
				$user->setFirstName($form->get('firstName')->getData());
				$user->setLastName($form->get('lastName')->getData());
				$user->setPhone($form->get('phone')->getData());
				$user->setUpdatedDate(new \DateTime());
				$em->persist($user);
				$em->flush();
			}

			return $this->redirectToRoute('fos_user_profile_show');

		}

		$response = new JsonResponse(
			array(
				'message' => 'Error',
				'form' => $this->renderView('SiteBundle:Default:index.html.twig',
					array(
						'entity' => $entity,
						'form' => $form->createView(),
					))), 400);

		return $response;

	}

	public  function updateUserDetailsAction(Request $request, $userId)
	{

		$entity = new UserDetails();
		$form = $this->createForm(new UserDetailsType(), $entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();

			$getUserName = $em->getRepository('AdminBundle:FosUser')
				->find($userId);

			$user = $em->getRepository('AdminBundle:UserDetails')
				->findOneBy(['fosUser' => $userId]);

			if (!$user instanceof UserDetails) {
				// INSERT
				$entity->setFirstName($form->get('firstName')->getData());
				$entity->setLastName($form->get('lastName')->getData());
				$entity->setPhone($form->get('phone')->getData());
				$entity->setFosUser($getUserName);
				$entity->setCreatedDate(new \DateTime());
				$em->persist($entity);
				$em->flush();
			} else {
				// UPDATE
				$user->setFirstName($form->get('firstName')->getData());
				$user->setLastName($form->get('lastName')->getData());
				$user->setPhone($form->get('phone')->getData());
				$user->setUpdatedDate(new \DateTime());
				$em->persist($user);
				$em->flush();
			}

			return $this->redirectToRoute('manage_users');

		}

	}

}
