<?php
/**
 * Created by Justin St. Germain
 * Date: 6/19/15
 * Time: 8:48 PM
 */

namespace Feedback\AdminBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Feedback\AdminBundle\Entity\FosUser;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Feedback\AdminBundle\Entity\FosUserHasBusiness;

class BusinessController extends Controller
{

	public function manageBusinessesAction()
	{

		if (false === $this->get('security.authorization_checker')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$business = $this->getDoctrine()
			->getRepository('AdminBundle:Business')
			->getAllBusinessesWithAvgRating();

		return $this->render('AdminBundle:Business:manageBusinesses.html.twig', array(
			'businesses'    => $business,
		));

	}

	public function manageBusinessesByIdAction($businessId)
	{

		if (false === $this->get('security.authorization_checker')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$business = $this->getDoctrine()
			->getRepository('AdminBundle:Business')
			->find($businessId);

		return $this->render('AdminBundle:Business:manageBusiness.html.twig', array(
			'business'      => $business,
			'business_id'   => $business->getId()
		));

	}


	public function getUsersByBusinessAction($businessId)
	{
		if (false === $this->get('security.authorization_checker')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$request = $this->get('request_stack')->getCurrentRequest();

		if ($request->isMethod('POST'))
		{

			$form = $request->request->get("data");

			$entity = new FosUserHasBusiness();

			$em = $this->getDoctrine()->getManager();

			if ($request->get('action') == 'remove')
			{

				foreach ( $request->get('id') as $rowId )
				{

					$userHasBusiness = $em->getRepository('AdminBundle:FosUserHasBusiness')
						->findOneBy(['id' => $rowId]);

					// Delete the relation for the user to business
					if ($userHasBusiness instanceof FosUserHasBusiness) {
						$em->remove($userHasBusiness);
						$em->persist($entity);
						$em->flush();
					}

				}

			} else {

				// GET THE ID FOR THE USER BEING INSERTED OR UPDATED/DELETED
				$userManager = $this->container->get('fos_user.user_manager');
				$user = $userManager->findUserByUsername($form['username']);

				$userId = $user->getId();

				$userHasBusiness = $em->getRepository('AdminBundle:FosUserHasBusiness')
					->findOneBy(['fosUser' => $userId, 'business' => $businessId]);

				$getUser = $em->getRepository('AdminBundle:FosUser')
					->find($userId);

				$getBusiness = $em->getRepository('AdminBundle:Business')
					->find($businessId);

				if ($user instanceof FosUser)
				{

					if (!$userHasBusiness instanceof FosUserHasBusiness) {
						// User isn't tied to the current business
						// Tie the user and business now
						$entity->setFosUser($getUser);
						$entity->setBusiness($getBusiness);
						$em->persist($entity);
						$em->flush();
					} else {
						echo 'hey fucker, don\'t add duplicates';
					}

				} else {
					echo 'that user does\'t exist dumbass';
				}

			}
//			die;

		}

		$users = $this->getDoctrine()
			->getRepository('AdminBundle:FosUserHasBusiness')
			->getBusinessUsersByBusinessId($businessId);

		$view = View::create()
			->setFormat('json')
			->setStatusCode(201)
			->setData([
				'data'  => $users
			])
		;

		return $this->get('fos_rest.view_handler')->handle($view);

	}

	public function manageMyBusinessesAction()
	{

		if (false === $this->get('security.authorization_checker')->isGranted(
				'IS_AUTHENTICATED_REMEMBERED'
			)) {
			throw new AccessDeniedException();
		}

		$business = $this->getDoctrine()
			->getRepository('AdminBundle:FosUserHasBusiness')
			->getBusinessesByUserId($this->getUser()->getId());

		return $this->render('AdminBundle:Business:manageMyBusinesses.html.twig', array(
			'businessList' => $business
		));

	}

}