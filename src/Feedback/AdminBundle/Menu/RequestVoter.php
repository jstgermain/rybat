<?php
/**
 * Created by Justin St. Germain
 * Date: 6/17/15
 * Time: 4:49 PM
 */

namespace Feedback\AdminBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestVoter implements VoterInterface
{

	private $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function matchItem(ItemInterface $item)
	{
		if ($item->getUri() === $this->container->get('request')->getRequestUri()) {
			return true;
		} else {
			if ($item->getUri() !== '/app_dev.php/admin/' && (substr($this->container->get('request')->getRequestUri(), 0, strlen($item->getUri())) === $item->getUri())) {
				return true;
			}
		}

		return null;
	}

}