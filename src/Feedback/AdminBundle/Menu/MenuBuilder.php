<?php
/**
 * Created by Justin St. Germain
 * Date: 6/15/15
 * Time: 7:45 PM
 */

namespace Feedback\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuBuilder
{

	private $factory;

	/**
	 * @param FactoryInterface $factory
	 */

	public function __construct(FactoryInterface $factory, ContainerInterface $container)
	{
		$this->factory = $factory;
		$this->container = $container;
	}

	public function createMainMenu(Request $request)
	{
		$menu = $this->factory->createItem('root');

		// Dashboard
		$menu->addChild('Dashboard', array('route' => 'admin'))
			->addChild('Manage Businesses', array('route' => 'manage_businesses'))->getParent()
			->addChild('Settings', array('route' => 'fos_user_profile_show'))->getParent()
			->addChild('Manage Users', array('route' => 'manage_users'))->getParent()
			->addChild('My Businesses', array('route' => 'manage_my_businesses'))->getParent();
//			->addChild('Edit Settings', array('route' => 'fos_user_profile_edit'))->getParent();
//			->addChild('Edit Business', array('route' => 'manage_business', 'routeParameters' => array('businessId' => $request->get('businessId'))))->getParent();

		return $menu;

	}


	public function createBreadcrumbsMenu(Request $request) {
		//
		$bcmenu = $this->createMainMenu($request);
		return $this->getCurrentMenuItem($bcmenu);
	}

	public function getCurrentMenuItem($menu)
	{
		$voter = $this->container->get('feedback.adminbundle.menu.voter.request');

		foreach ($menu as $item) {
			if ($voter->matchItem($item)) {
				return $item;
			}

			if ($item->getChildren() && $currentChild = $this->getCurrentMenuItem($item)) {
				return $currentChild;
			}
		}

		return null;
	}

}