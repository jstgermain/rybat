<?php

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FosUserHasBusiness
 *
 * @ORM\Table(name="fos_user_has_business", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity(repositoryClass="Feedback\AdminBundle\Entity\FosUserHasBusinessRepository")
 */
class FosUserHasBusiness
{
	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \Feedback\AdminBundle\Entity\FosUser
	 *
	 * @ORM\ManyToOne(targetEntity="Feedback\AdminBundle\Entity\FosUser")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="fos_user_id", referencedColumnName="id")
	 * })
	 */
	private $fosUser;

	/**
	 * @var \Feedback\AdminBundle\Entity\Business
	 *
	 * @ORM\ManyToOne(targetEntity="Feedback\AdminBundle\Entity\Business")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="business_id", referencedColumnName="id")
	 * })
	 */
	private $business;


	/**
	 * Set id
	 *
	 * @param integer $id
	 * @return FosUserHasBusiness
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set fosUser
	 *
	 * @param \Feedback\AdminBundle\Entity\FosUser $fosUser
	 * @return FosUserHasBusiness
	 */
	public function setFosUser(\Feedback\AdminBundle\Entity\FosUser$fosUser)
	{
		$this->fosUser = $fosUser;

		return $this;
	}

	/**
	 * Get fosUser
	 *
	 * @return \Feedback\AdminBundle\Entity\FosUser
	 */
	public function getFosUser()
	{
		return $this->fosUser;
	}

	/**
	 * Set business
	 *
	 * @param \Feedback\AdminBundle\Entity\Business $business
	 * @return FosUserHasBusiness
	 */
	public function setBusiness(\Feedback\AdminBundle\Entity\Business $business)
	{
		$this->business = $business;

		return $this;
	}

	/**
	 * Get business
	 *
	 * @return \Feedback\AdminBundle\Entity\Business
	 */
	public function getBusiness()
	{
		return $this->business;
	}
}
