<?php
/**
 * Created by Justin St. Germain
 * Date: 6/10/15
 * Time: 7:30 AM
 */

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CommentRepository extends EntityRepository
{

	public function getAllCommentsByBusinessId($businessId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select(["comment", "business"])
			->from("AdminBundle:Comment", "comment")
			->innerJoin("comment.business", "business")
			->where("business.id = :id")
			->setParameter("id", $businessId);
		return $queryBuilder->getQuery()->getResult();
	}

}