<?php

namespace Feedback\AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * FosUser
 *
 * @ORM\Table(name="fos_user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_957A647992FC23A8", columns={"username_canonical"}), @ORM\UniqueConstraint(name="UNIQ_957A6479A0D96FBF", columns={"email_canonical"})})
 * @ORM\Entity(repositoryClass="Feedback\AdminBundle\Entity\FosUserRepository")
 */
class FosUser extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @ORM\ManyToMany(targetEntity="Feedback\AdminBundle\Entity\FosGroup")
	 * @ORM\JoinTable(name="fos_user_user_group",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
	 * )
	 */
	protected $groups;

	/**
	 * @var \Doctrine\Common\Collections\Collection
	 *
	 * @ORM\ManyToMany(targetEntity="Feedback\AdminBundle\Entity\Business", inversedBy="fosUser")
	 * @ORM\JoinTable(name="fos_user_has_business",
	 *   joinColumns={
	 *     @ORM\JoinColumn(name="fos_user_id", referencedColumnName="id")
	 *   },
	 *   inverseJoinColumns={
	 *     @ORM\JoinColumn(name="business_id", referencedColumnName="id")
	 *   }
	 * )
	 */
	private $business;

	public function __construct()
	{
		parent::__construct();
		// your own logic
	}


	/**
	 * Add business
	 *
	 * @param \Feedback\AdminBundle\Entity\Business $business
	 * @return FosUser
	 */
	public function addBusiness(\Feedback\AdminBundle\Entity\Business $business)
	{
		$this->business[] = $business;

		return $this;
	}

	/**
	 * Remove business
	 *
	 * @param \Feedback\AdminBundle\Entity\Business $business
	 */
	public function removeBusiness(\Feedback\AdminBundle\Entity\Business $business)
	{
		$this->business->removeElement($business);
	}

	/**
	 * Get business
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getBusiness()
	{
		return $this->business;
	}
}
