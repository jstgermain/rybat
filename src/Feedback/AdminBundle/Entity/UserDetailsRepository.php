<?php
/**
 * Created by Justin St. Germain
 * Date: 7/8/15
 * Time: 5:44 PM
 */

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserDetailsRepository extends EntityRepository
{

	public function getAllUsers()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select("u")
			->from("AdminBundle:UserDetails", "u");
		return $queryBuilder->getQuery()->getResult();
	}

	public function getAllUsernames()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user");
		$queryBuilder->select(
			"fos_user.username"
		)
			->from("AdminBundle:FosUser", "fos_user")
		;
		return $queryBuilder->getQuery()->getResult();
	}

	public function getAllUsersWithDetails()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user", "user_details");
		$queryBuilder->select(
			"fos_user.id",
			"fos_user.username",
			"user_details.firstName",
			"user_details.lastName",
			"user_details.phone"
		)
			->from("AdminBundle:FosUser", "fos_user")
			->leftJoin("AdminBundle:UserDetails", "user_details")
			->where("user_details.fosUser = fos_user.id")
			->groupBy('fos_user.id')
			->orderBy("fos_user.id", "ASC")
		;
		return $queryBuilder->getQuery()->getResult();
	}


	public function getUserDetailsById($fosUser)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user", "user_details");
		$queryBuilder->select(
			"fos_user.id"
			, "fos_user.username"
			, "user_details.firstName"
			, "user_details.lastName"
			, "user_details.phone"
		)
			->from("AdminBundle:FosUser", "fos_user")
			->leftJoin("AdminBundle:UserDetails", "user_details", "WITH", "user_details.fosUser = :id")
			->where("fos_user.id = :id")
			->setParameter("id", $fosUser)
		;
		return $queryBuilder->getQuery()->getOneOrNullResult();
	}


	public function getUsernameFromId($userId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user");
		$queryBuilder->select(
			"fos_user.username"
		)
			->from("AdminBundle:FosUser", "fos_user")
			->where("fos_user.id = :id")
			->setParameter("id", $userId)
		;
		return $queryBuilder->getQuery()->getOneOrNullResult();
	}

}