<?php

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BusinessRatings
 *
 * @ORM\Table(name="business_ratings", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity(repositoryClass="Feedback\AdminBundle\Entity\BusinessRatingsRepository")
 */
class BusinessRatings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="rated_by", type="integer", nullable=true)
     */
    private $ratedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_date", type="datetime", nullable=true)
     */
    private $deletedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Feedback\AdminBundle\Entity\Business
     *
     * @ORM\ManyToOne(targetEntity="Feedback\AdminBundle\Entity\Business")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     * })
     */
    private $business;



    /**
     * Set rating
     *
     * @param integer $rating
     * @return BusinessRatings
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set ratedBy
     *
     * @param integer $ratedBy
     * @return BusinessRatings
     */
    public function setRatedBy($ratedBy)
    {
        $this->ratedBy = $ratedBy;

        return $this;
    }

    /**
     * Get ratedBy
     *
     * @return integer 
     */
    public function getRatedBy()
    {
        return $this->ratedBy;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return BusinessRatings
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     * @return BusinessRatings
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime 
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set deletedDate
     *
     * @param \DateTime $deletedDate
     * @return BusinessRatings
     */
    public function setDeletedDate($deletedDate)
    {
        $this->deletedDate = $deletedDate;

        return $this;
    }

    /**
     * Get deletedDate
     *
     * @return \DateTime 
     */
    public function getDeletedDate()
    {
        return $this->deletedDate;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set business
     *
     * @param \Feedback\AdminBundle\Entity\Business $business
     * @return BusinessRatings
     */
    public function setBusiness(\Feedback\AdminBundle\Entity\Business $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Feedback\AdminBundle\Entity\Business
     */
    public function getBusiness()
    {
        return $this->business;
    }
}
