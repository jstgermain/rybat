<?php
/**
 * Created by Justin St. Germain
 * Date: 7/20/15
 * Time: 9:20 AM
 */

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FosUserHasBusinessRepository extends EntityRepository
{

	public function getBusinessUsersByBusinessId($businessId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user_bus_xref", "fos_user", "business", "user_details");
		$queryBuilder->select(
			"fos_user_bus_xref.id as DT_RowId",
			"fos_user.username",
			"user_details.firstName",
			"user_details.lastName",
			"user_details.phone"
		)
			->from("AdminBundle:FosUserHasBusiness", "fos_user_bus_xref")
			->innerJoin("fos_user_bus_xref.fosUser", "fos_user")
			->where("fos_user_bus_xref.fosUser = fos_user.id")
			->leftJoin("AdminBundle:UserDetails", "user_details", "WITH", "user_details.fosUser = fos_user.id")
			->innerJoin("fos_user_bus_xref.business", "business")
			->where("fos_user_bus_xref.business = :businessId")
			->setParameter("businessId", $businessId)
		;
		return $queryBuilder->getQuery()->getResult();
	}

	public function getBusinessesByUserId($userId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("fos_user_bus_xref", "business");
		$queryBuilder->select(
			"business.name",
			"business.address1",
			"business.address2",
			"business.city",
			"business.state",
			"business.zip",
			"business.appleMapId",
			"business.latitude",
			"business.longitude",
			"business.phone"
		)
			->from("AdminBundle:FosUserHasBusiness", "fos_user_bus_xref")
			->innerJoin("fos_user_bus_xref.business", "business")
			->where("fos_user_bus_xref.fosUser = :userId")
			->setParameter("userId", $userId)
		;
		return $queryBuilder->getQuery()->getResult();
	}

}