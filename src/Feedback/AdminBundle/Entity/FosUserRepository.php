<?php
/**
 * Created by Justin St. Germain
 * Date: 6/10/15
 * Time: 7:30 AM
 */

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FosUserRepository extends EntityRepository
{

	public function getUsersWithBusinessById($businessId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select(["fosUser", "business"])
			->from("AdminBundle:FosUser", "fosUser")
			->innerJoin("fosUser.business", "business")
			->where("business.id = :id")
			->setParameter("id", $businessId);
		return $queryBuilder->getQuery()->getResult();
	}

	public function getUsersIdByUsername($userName)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select(["fosUser"])
			->from("AdminBundle:FosUser", "fosUser")
			->where("fosUser.username = :username")
			->setParameter("username", $userName);
		return $queryBuilder->getQuery()->getResult();
	}

	public function getUsernameById($userId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select(["fosUser"])
			->from("AdminBundle:FosUser", "fosUser")
			->where("fosUser.id = :id")
			->setParameter("id", $userId);
		return $queryBuilder->getQuery()->getResult();
	}

}