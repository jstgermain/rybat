<?php
/**
 * Created by Justin St. Germain
 * Date: 6/3/15
 * Time: 12:47 PM
 */

namespace Feedback\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class BusinessRepository extends EntityRepository
{

	/**
	 * ADMINISTRATOR FUNCTIONS FOR MANAGING BUSINESSES
	 */

	public function getAllBusinesses()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select("b")
			->from("AdminBundle:Business", "b");
		return $queryBuilder->getQuery()->getResult();
	}

	public function getBusinessById($businessId)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select("business")
			->from("AdminBundle:Business", "business")
			->where("business.id = :id")
			->setParameter("id", $businessId);
		return $queryBuilder->getQuery()->getOneOrNullResult();
	}

	public function getAllBusinessesWithAvgRating()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder("business", "business_ratings");
		$queryBuilder->select(
			"avg(business_ratings.rating) as avg_rating",
			"business.id",
			"business.name",
			"business.address1",
			"business.address2",
			"business.appleMapId",
			"business.city",
			"business.createdDate",
			"business.googleMapId",
			"business.latitude",
			"business.longitude",
			"business.phone",
			"business.state",
			"business.zip"
		)
			->from("AdminBundle:Business", "business")
			->leftJoin("AdminBundle:BusinessRatings", "business_ratings")
			->where("business_ratings.business = business.id")
			->groupBy('business.id')
		;
		return $queryBuilder->getQuery()->getResult();
	}

}