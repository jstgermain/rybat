<?php
/**
 * Created by Justin St. Germain
 * Date: 6/9/15
 * Time: 8:29 PM
 */

namespace Feedback\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Feedback\AdminBundle\Entity\Business;

class LoadDefaultBusinesses implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.494740');
		$business->setLongitude('-111.926460');
		$business->setAppleMapId('abc123');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('4032 N Scottsdale Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85251');
		$business->setPhone('(480) 947-4578');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.493670');
		$business->setLongitude('-111.907460');
		$business->setAppleMapId('def456');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('8035 E Indian School Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85251');
		$business->setPhone('(480) 946-6821');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.485700');
		$business->setLongitude('-111.909350');
		$business->setAppleMapId('def789');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('3300 N Hayden Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85251');
		$business->setPhone('(480) 946-2488');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.479880');
		$business->setLongitude('-111.925610');
		$business->setAppleMapId('ghi123');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('2865 N Scottsdale Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85257');
		$business->setPhone('(480) 947-5227');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.510670');
		$business->setLongitude('-111.910350');
		$business->setAppleMapId('ghi456');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('7920 E Chaparral Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85250');
		$business->setPhone('(480) 994-5653');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.526020');
		$business->setLongitude('-111.924570');
		$business->setAppleMapId('ghi789');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('6137 N Scottsdale Rd');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85250');
		$business->setPhone('(480) 483-5698');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Starbucks');
		$business->setLatitude('33.539350');
		$business->setLongitude('-111.923910');
		$business->setAppleMapId('jkl123');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('7001 N Scottsdale Rd');
		$business->setAddress2('Suite 101');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85258');
		$business->setPhone('(480) 348-0056');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Lox Stock & Bagel');
		$business->setLatitude('33.543570');
		$business->setLongitude('-111.904350');
		$business->setAppleMapId('abc456');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('7353 N Vía Paseo Del Sur');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85258');
		$business->setPhone('(480) 922-0123');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Flair Gifts & Boutique');
		$business->setLatitude('33.751530');
		$business->setLongitude('-111.991290');
		$business->setAppleMapId('abc789');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('29455 N Cave Creek Rd');
		$business->setAddress2('Suite 122');
		$business->setCity('Cave Creek');
		$business->setState('AZ');
		$business->setZip('85331');
		$business->setPhone('(480) 515-3883');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Lotions & Potions');
		$business->setLatitude('33.425760');
		$business->setLongitude('-111.940370');
		$business->setAppleMapId('def123');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('420 S Mill Ave');
		$business->setAddress2('Suite 105');
		$business->setCity('Tempe');
		$business->setState('AZ');
		$business->setZip('85281');
		$business->setPhone('(480) 968-4652');
		$manager->persist($business);

		$business = new Business();
		$business->setName('Justin\'s Awesome Store');
		$business->setLatitude('33.520770');
		$business->setLongitude('-111.895754');
		$business->setAppleMapId('mno123');
		$business->setCreatedDate(new \DateTime());
		$business->setAddress1('8613 E Solano Dr');
		$business->setCity('Scottsdale');
		$business->setState('AZ');
		$business->setZip('85250');
		$business->setPhone('(480) 233-5006');
		$manager->persist($business);

		// RUN ALL QUERIES
		$manager->flush();

	}

}
