<?php
/**
 * Created by Justin St. Germain
 * Date: 6/9/15
 * Time: 7:49 PM
 */

namespace Feedback\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDefaultUsers implements FixtureInterface, ContainerAwareInterface
{

	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * {@inheritDoc}
	 */
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
    }

	public function load(ObjectManager $manager)
	{
		$userManager = $this->container->get('fos_user.user_manager');

		$user = $userManager->createUser();
        $user->setUsername('jstgermain');
        $user->setEmail('justin@ibrightdev.com');
        $user->setPlainPassword('c0mpt3ch');
        $user->setEnabled(true);

        $manager->persist($user);
        $manager->flush();
	}

}