<?php

namespace Feedback\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table(name="newsletter")
 * @ORM\Entity(repositoryClass="Feedback\SiteBundle\Entity\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_date", type="datetime", nullable=false)
	 */
	private $createdDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_date", type="datetime", nullable=true)
	 */
	private $updatedDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deleted_date", type="datetime", nullable=true)
	 */
	private $deletedDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Newsletter
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

	/**
	 * Set createdDate
	 *
	 * @param \DateTime $createdDate
	 * @return Newsletter
	 */
	public function setCreatedDate($createdDate)
	{
		$this->createdDate = $createdDate;

		return $this;
	}

	/**
	 * Get createdDate
	 *
	 * @return \DateTime
	 */
	public function getCreatedDate()
	{
		return $this->createdDate;
	}

	/**
	 * Set updatedDate
	 *
	 * @param \DateTime $updatedDate
	 * @return Newsletter
	 */
	public function setUpdatedDate($updatedDate)
	{
		$this->updatedDate = $updatedDate;

		return $this;
	}

	/**
	 * Get updatedDate
	 *
	 * @return \DateTime
	 */
	public function getUpdatedDate()
	{
		return $this->updatedDate;
	}

	/**
	 * Set deletedDate
	 *
	 * @param \DateTime $deletedDate
	 * @return Newsletter
	 */
	public function setDeletedDate($deletedDate)
	{
		$this->deletedDate = $deletedDate;

		return $this;
	}

	/**
	 * Get deletedDate
	 *
	 * @return \DateTime
	 */
	public function getDeletedDate()
	{
		return $this->deletedDate;
	}

}
