'use strict'; 
/*! main.js - v0.1.1
* http://admindesigns.com/
* Copyright (c) 2015 Admin Designs;*/


/* Demo theme functions. Required for
 * Settings Pane and misc functions */
var demoHighCharts = function () {

        // Define chart color patterns
        var highColors = [bgWarning, bgPrimary, bgInfo, bgAlert,
            bgDanger, bgSuccess, bgSystem, bgDark
        ];

        // Color Library we used to grab a random color
        var sparkColors = {
            "primary": [bgPrimary, bgPrimaryLr, bgPrimaryDr],
            "info": [bgInfo, bgInfoLr, bgInfoDr],
            "warning": [bgWarning, bgWarningLr, bgWarningDr],
            "success": [bgSuccess, bgSuccessLr, bgSuccessDr],
            "alert": [bgAlert, bgAlertLr, bgAlertDr]
        };


        // High Charts Demo
        var demoHighCharts = function() {

            var demoHighLines = function() {

                var d = new Date();

                var month = d.getMonth(),
                    day = d.getDate() - 6, // ONLY -6 WHEN WANTING TODAY AND THE PREVIOUS 6
                    year = d.getFullYear();

                var date = new Date(year, month, day, 0,0,0,0);
                const DAY = 1000 * 60 * 60 * 24;

                var dateRange = [];

                var i;
                for (i = 0; i <= 6; i++) {
                    dateRange.push( $.datepicker.formatDate('m/d/y', new Date(date)) );
                    date.setTime(date.getTime() + DAY);
                }

                //console.log(dateRange);

                var line3 = $('#high-line3');

                if (line3.length) {

                    // High Line 3
                    $('#high-line3').highcharts({
                        credits: false,
                        colors: highColors,
                        chart: {
                            backgroundColor: 'transparent',
                            className: '',
                            type: 'line',
                            zoomType: 'x',
                            panning: true,
                            panKey: 'shift',
                            marginTop: 45,
                            marginRight: 1,
                        },
                        title: {
                            text: null
                        },
                        xAxis: {
                            gridLineColor: '#EEE',
                            lineColor: '#EEE',
                            tickColor: '#EEE',
                            categories: dateRange
                        },
                        yAxis: {
                            min: 0,
                            tickInterval: 5,
                            gridLineColor: '#EEE',
                            title: {
                                text: null,
                            }
                        },
                        plotOptions: {
                            spline: {
                                lineWidth: 3,
                            },
                            area: {
                                fillOpacity: 0.2
                            }
                        },
                        legend: {
                            enabled: true,
                            floating: false,
                            align: 'right',
                            verticalAlign: 'top',
                            x: -15
                        },
                        series: [{
                            name: 'Yahoo',
                            data: [7.0, 6, 9, 14, 18, 21.5, 25.2]
                        }, {
                            name: 'CNN',
                            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0]
                        }, {
                            visible: false,
                            name: 'Yahoo',
                            data: [1, 5, 5.7, 11.3, 17.0, 22.0, 24.8]
                        }, {
                            //visible: false,
                            name: 'Facebook',
                            data: [3, 1, 3.5, 8.4, 13.5, 17.0, 18.6]
                        }, {
                            visible: false,
                            name: 'Facebook',
                            data: [7.0, 6, 9, 14, 18, 21.5, 25.2]
                        }, {
                            visible: false,
                            name: 'CNN',
                            data: [1, 5, 5.7, 11.3, 17.0, 22.0, 24.8]
                        }]
                    });

                }

            } // End High Line Charts Demo

            // Init Chart Types
            demoHighLines();

        }; // End Demo HighCharts

	return {
        init: function () {

            // Init Demo Charts 
            demoHighCharts();

        }
	} 
}();










