'use strict';
//  Author: Justin St. Germain
// 
//  This file is reserved for changes made by the use. 
//  Always seperate your work from the theme. It makes
//  modifications, and future theme updates much easier 
// 

(function($) {

	// Prevents directory response when submitting a demo form
	//$('#newsletter-subscribe').submit(function(e) {
	//	e.preventDefault();
	//
	//	return false;
	//
	//});


	$('.admin-form').on('submit', '#newsletter-subscribe', function(e) {
		e.preventDefault();

		var form = $(this);

		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize()
		})
		.done(function (data) {
			if (typeof data.message !== 'undefined') {
				//form[0].reset();
				//alert(data.message);
				$('.alert')
					.html("Success: Thank you for subscribing to be notified once we launch.")
					.removeClass('hidden')
					.addClass('active')
					.addClass('alert-success')
					.animate({opacity: 1}, 1000, function() {
						form[0].reset();
						$('.alert')
							.delay(2000)
							.animate({opacity: 0}, 1000, function() {
								$('.alert')
									.html("")
									.removeClass('active')
									.removeClass('alert-success')
									.addClass('hidden')
								;
							})
						;
					})
				;
			}
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			if (typeof jqXHR.responseJSON !== 'undefined') {
				if (jqXHR.responseJSON.hasOwnProperty('form')) {
					$('#form_body').html(jqXHR.responseJSON.form);
				}

				$('.form_error').html(jqXHR.responseJSON.message);

			} else {
				$('.alert')
					.html("Error: Uh oh! You broke our form. Please try again later.")
					.removeClass('hidden')
					.addClass('active')
					.addClass('alert-error')
					.animate({opacity: 1}, 1000, function() {
						$('.alert')
							.delay(2000)
							.animate({opacity: 0}, 1000, function() {
								$('.alert')
									.html("")
									.removeClass('active')
									.removeClass('alert-error')
									.addClass('hidden')
								;
							})
						;
					})
				;
			}

		});
	});


	//$(document).on('submit', '#profile-edit-form', function(e) {
	//	e.preventDefault();
	//
	//	var form = $(this);
	//
	//	$.ajax({
	//		type: $(this).attr('method'),
	//		url: $(this).attr('action'),
	//		data: $(this).serialize()
	//	})
	//	.done(function (data) {
	//		if (typeof data.message !== 'undefined') {
	//			//form[0].reset();
	//			//alert(data.message);
	//			$('.alert')
	//				.html("Success: User details have been updated.")
	//				.removeClass('hidden')
	//				.addClass('active')
	//				.addClass('alert-success')
	//				.animate({opacity: 1}, 1000, function() {
	//					form[0].reset();
	//					$('.alert')
	//						.delay(2000)
	//						.animate({opacity: 0}, 1000, function() {
	//							$('.alert')
	//								.html("")
	//								.removeClass('active')
	//								.removeClass('alert-success')
	//								.addClass('hidden')
	//							;
	//						})
	//					;
	//				})
	//			;
	//		}
	//	})
	//	.fail(function (jqXHR, textStatus, errorThrown) {
	//		if (typeof jqXHR.responseJSON !== 'undefined') {
	//			if (jqXHR.responseJSON.hasOwnProperty('form')) {
	//				$('#form_body').html(jqXHR.responseJSON.form);
	//			}
	//
	//			$('.form_error').html(jqXHR.responseJSON.message);
	//
	//		} else {
	//			$('.alert')
	//				.html("Error: There was a problem processing your update.")
	//				.removeClass('hidden')
	//				.addClass('active')
	//				.addClass('alert-error')
	//				.animate({opacity: 1}, 1000, function() {
	//					$('.alert')
	//						.delay(2000)
	//						.animate({opacity: 0}, 1000, function() {
	//							$('.alert')
	//								.html("")
	//								.removeClass('active')
	//								.removeClass('alert-error')
	//								.addClass('hidden')
	//							;
	//						})
	//					;
	//				})
	//			;
	//		}
	//
	//	});
	//});


	//$('#login-form').submit(function(e) {
	//	e.preventDefault();
	//
	//	var form = $(this),
	//		processUrl = form.attr('action'),
	//		formData = form.serialize(),
	//		formMethod = form.attr('method');
	//
	//	//console.log(processUrl);
	//
	//	$.ajax({
	//		type: formMethod,
	//		url: processUrl,
	//		data: formData,
	//		success: function() {
	//			$('.alert')
	//				.html("Success: Your message has been received. Someone will be in touch with you shortly.")
	//				.removeClass('hidden')
	//				.addClass('active')
	//				.addClass('alert-success')
	//				.animate({opacity: 1}, 1000, function() {
	//					form[0].reset();
	//					$('.alert')
	//						.delay(2000)
	//						.animate({opacity: 0}, 1000, function() {
	//							$('.alert')
	//								.html("")
	//								.removeClass('active')
	//								.removeClass('alert-success')
	//								.addClass('hidden')
	//							;
	//						})
	//					;
	//				})
	//			;
	//		},
	//		error: function() {
	//			$('.alert')
	//				.html("Error: Uh oh! You broke our contact form. Please try again later.")
	//				.removeClass('hidden')
	//				.addClass('active')
	//				.addClass('alert-error')
	//				.animate({opacity: 1}, 1000, function() {
	//					$('.alert')
	//						.delay(2000)
	//						.animate({opacity: 0}, 1000, function() {
	//							$('.alert')
	//								.html("")
	//								.removeClass('active')
	//								.removeClass('alert-error')
	//								.addClass('hidden')
	//							;
	//						})
	//					;
	//				})
	//			;
	//		}
	//	});
	//
	//});





})(jQuery);

