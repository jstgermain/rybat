<?php
/**
 * Created by Justin St. Germain
 * Date: 6/12/15
 * Time: 8:43 PM
 */

namespace Feedback\SiteBundle\Controller;

use Feedback\SiteBundle\Entity\Newsletter;
use Feedback\SiteBundle\Form\NewsletterType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FormProcessController  extends FOSRestController
{

	/**
	 * @Post("/newsletter")
	 */
	public function postNewsletterSubscribeAction(Request $request)
	{

		//This is optional. Do not do this check if you want to call the same action using a regular request.
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		$entity = new Newsletter();
		$form = $this->createForm(new NewsletterType(), $entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity->setCreatedDate(new \DateTime());
			$em->persist($entity);
			$em->flush();

		    //Redirect the user and add a thank you message
		    $message = \Swift_Message::newInstance()
			    ->setSubject('Feedback International - Newsletter Signup')
			    ->setTo($this->container->getParameter('send_emails_to'))
			    ->setFrom(array($this->container->getParameter('swiftmailer.sender_address') => 'Feedback International'))
			    ->setBody(
				    $this->renderView(
					    'SiteBundle:Mail:subscribe.html.twig',
					    array(
						    'ip' => $request->getClientIp(),
						    'email' => $form->get('email')->getData()
					    )
				    )
			    );

		    $this->get('mailer')->send($message);

			return new JsonResponse(array('message' => 'Success!'), 200);
		}

		$response = new JsonResponse(
			array(
				'message' => 'Error',
				'form' => $this->renderView('SiteBundle:Default:index.html.twig',
					array(
						'entity' => $entity,
						'form' => $form->createView(),
					))), 400);

		return $response;

	}

}
