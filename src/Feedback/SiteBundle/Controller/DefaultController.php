<?php

namespace Feedback\SiteBundle\Controller;

use Feedback\AdminBundle\Entity\Business;
use Feedback\SiteBundle\Entity\Newsletter;
use Feedback\AdminBundle\Form\BusinessType;
use Feedback\SiteBundle\Form\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {

	    $business = $this->getDoctrine()
		    ->getRepository('AdminBundle:Business')
		    ->getAllBusinesses();
//		    ->getBusinessById(10913);
//	    var_dump($business); die;
//	    foreach ( $business as $bus )
//	    {
//		    /** @var Business $bus */
//		    echo $bus->getBusiness()->getId() . ', ' . $bus->getBusiness()->getName() . ', ' . $bus->getAddress1() . '<br/>';
//	    }

	    //Create a new subscribe entity instance
//	    $businessForm = new Business();
//	    $addBusiness = $this->createForm(new BusinessType(), $businessForm);


	    //Create a new subscribe entity instance
	    $subscribe = new Newsletter();
	    $subscribeNewsletter = $this->createForm(new NewsletterType(), $subscribe);


	    return $this->render('SiteBundle:Default:index.html.twig', array(
		    'subscribe' => $subscribeNewsletter->createView(),
	    ));
    }
}
