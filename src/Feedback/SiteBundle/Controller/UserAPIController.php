<?php

namespace Feedback\SiteBundle\Controller;

use Feedback\AdminBundle\Entity\Business;
use Feedback\AdminBundle\Entity\BusinessRatings;
use Feedback\AdminBundle\Entity\Comment;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

class UserAPIController extends FOSRestController
{

	/**
	 * @Get("/user/{businessId}")
	 */
    public function apiGetUserHasBusinessByIdAction($businessId)
    {
	    $user = $this->getDoctrine()
		    ->getRepository('AdminBundle:FosUser')
		    ->getUsersWithBusinessById($businessId);

	    $view = View::create()
		    ->setFormat('json')
		    ->setStatusCode(200)
		    ->setData(['user' => $user])
	    ;

	    return $this->get('fos_rest.view_handler')->handle($view);
    }

}
